﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Bloodbender
{
    class RandomGarbage : PhysicObj
    {
        public RandomGarbage(Vector2 pos) : base(pos)
        {
            Random rnd = new Random();
            int nbr = rnd.Next(1, 4);
            if (nbr == 1)
                triangle1();
            else if (nbr == 2)
                triangle2();
            else
                triangle3();
            body.FixedRotation = false;
            addAnimation(new Animation(Bloodbender.ptr.Content.Load<Texture2D>("canPart")));
            Bloodbender.ptr.listGraphicObj.Add(this);
        }

        public Fixture triangle1()
        {
            Vertices mapVertices = new Vertices();

            mapVertices.Add(new Vector2(0 - 4, 7 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(7 - 4, 7 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(7 - 4, 1 - 4) * Bloodbender.pixelToMeter);
            PolygonShape shape = new PolygonShape(mapVertices, 20);
            Fixture fix = body.CreateFixture(shape);
            fix.UserData = new AdditionalFixtureData(this, HitboxType.BOUND);
            return fix;
        }

        public Fixture triangle2()
        {
            Vertices mapVertices = new Vertices();

            mapVertices.Add(new Vector2(0 - 4, 1 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(7 - 4, 7 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(3 - 4, 1 - 4) * Bloodbender.pixelToMeter);
            PolygonShape shape = new PolygonShape(mapVertices, 20);
            Fixture fix = body.CreateFixture(shape);
            fix.UserData = new AdditionalFixtureData(this, HitboxType.BOUND);
            return fix;
        }
        public Fixture triangle3()
        {
            Vertices mapVertices = new Vertices();

            mapVertices.Add(new Vector2(0 - 4, 0 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(0 - 4, 7 - 4) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(3 - 4, 1 - 4) * Bloodbender.pixelToMeter);
            PolygonShape shape = new PolygonShape(mapVertices, 20);
            Fixture fix = body.CreateFixture(shape);
            fix.UserData = new AdditionalFixtureData(this, HitboxType.BOUND);
            return fix;
        }
    }
}
