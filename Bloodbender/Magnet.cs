﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Bloodbender
{
    public class Magnet : PhysicObj
    {
        Fixture boundFix;
        Fixture sensorFix;
        private float influenceRadius;
        private float force;
        private bool activated;
        private bool reversed;
        private bool polarityForced;

        public Wave wave;
        public GraphicObj range;

        enum Anim
        {
            Minus = 0,Plus = 1,FromMinusToPlus= 2,FromPlusToMinus =3
        };

        public Magnet(Vector2 position, float influenceRadius, float force) : base(position)
        {
            polarityForced = false;
            activated = false;
            reversed = false;
            this.influenceRadius = influenceRadius;
            this.force = force;
            //boundFix = createCircleFixture(1f, Vector2.Zero, 1, new AdditionalFixtureData(this, HitboxType.BOUND));
            setBodyType(BodyType.Static);
            sensorFix = createCircleFixture(influenceRadius, Vector2.Zero, 0, new AdditionalFixtureData(this, HitboxType.ATTACK));

            sensorFix.IsSensor = true;

            addFixtureToCheckedCollision(sensorFix);
            //addFixtureToCheckedCollision(boundFix);

            Texture2D plustominus = Bloodbender.ptr.Content.Load<Texture2D>("fromPlusToMinus");
            Texture2D moinstoplus = Bloodbender.ptr.Content.Load<Texture2D>("fromMinusToPlus");
            Texture2D minus = Bloodbender.ptr.Content.Load<Texture2D>("magnetMinus");
            Texture2D plus = Bloodbender.ptr.Content.Load<Texture2D>("magnetPlus");

            Animation anim1 = new Animation(minus);
            anim1.isLooping = false;
            anim1.forceDepth(0.8f);
            Animation anim2 = new Animation(plus);
            anim2.isLooping = false;
            anim2.forceDepth(0.8f);
            Animation anim3 = new Animation(moinstoplus, 6, 0.05f);
            anim3.isLooping = false;
            anim3.forceDepth(0.8f);
            Animation anim4 = new Animation(plustominus, 6, 0.05f);
            anim4.isLooping = false;
            anim4.forceDepth(0.8f);

            addAnimation(anim1);
            addAnimation(anim2);
            addAnimation(anim3);
            addAnimation(anim4);

            runAnimation((int)Anim.Plus);

            wave = new Wave(GraphicObj.OffSet.Center);
            wave.linkTo(this);

            range = new GraphicObj(GraphicObj.OffSet.Center);
            range.linkTo(this);
            Bloodbender.ptr.listGraphicObj.Add(range);

            Texture2D rangeTexture = Bloodbender.ptr.Content.Load<Texture2D>("range");

            Animation rangeAnim = new Animation(rangeTexture);
            range.addAnimation(rangeAnim);
            range.runAnimation(0);
        }

        public override bool Update(float elapsed)
        {
            Vector2 cursorPosition = Bloodbender.ptr.inputHelper.Cursor;
            cursorPosition = Bloodbender.ptr.camera.ConvertScreenToWorld(cursorPosition);
            if (sensorFix.TestPoint(ref cursorPosition))
            {
                range.getAnimation(range.getCurrentAnimation()).show = true;


                if (Bloodbender.ptr.inputHelper.IsNewMouseButtonPress(MouseButtons.LeftButton))
                {
                    SoundEffectInstance attractInstance = Bloodbender.ptr.attractCanEffect.CreateInstance();
                    attractInstance.Volume = 0.12f;
                    attractInstance.Play();
                    activated = !activated;
                }
                else if (Bloodbender.ptr.inputHelper.IsNewMouseButtonPress(MouseButtons.RightButton) && polarityForced == false)
                {
                    SoundEffectInstance repulseInstance = Bloodbender.ptr.repulseCanEffect.CreateInstance();
                    repulseInstance.Volume = 0.22f;
                    repulseInstance.Play();
                    reversed = !reversed;
                    if (reversed == true)
                    {
                        wave.runAnimation(1);
                        runAnimation((int)Anim.FromPlusToMinus);

                    }
                    else
                    {
                        wave.runAnimation(0);
                        runAnimation((int)Anim.FromMinusToPlus);

                    }
                }
            }
            else
                range.getAnimation(range.getCurrentAnimation()).show = false;

            wave.getAnimation(wave.getCurrentAnimation()).show = activated;

            if (reversed)
            {
                if (!isAnimationRunning((int)Anim.FromPlusToMinus))
                    runAnimation((int)Anim.Minus);
            }
            else
            {
                if (!isAnimationRunning((int)Anim.FromMinusToPlus))
                    runAnimation((int)Anim.Plus);
            }
                    

            for (int i = 0; i < animations.Count; ++i)
            {
                //Console.WriteLine(i + ": " + animations[i].isLooping + " " + animations[i].isRunning);
            }

            checkSensorInteractions();
            return base.Update(elapsed);
        }

        public void reverseAttraction()
        {
            reversed = !reversed; 
        }

        public void setPolarityForced(bool forced)
        {
            polarityForced = forced;
        }

        private void applyAttraction(Body attractedBody)
        {
            Vector2 distance = new Vector2(0, 0);
            distance += attractedBody.Position;
            distance -= body.Position;
            if (!reversed)
                distance *= -1;
            float vecSum = Math.Abs(distance.X) + Math.Abs(distance.Y);
            float finalDistance = distance.Length();
            //if (distance.Length() < 0.2f)
                //finalDistance = 0.3f;
            distance *= (1.0f / vecSum) * force / finalDistance;
            attractedBody.ApplyForce(distance);
        }
        
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        private void checkSensorInteractions()
        {
            AdditionalFixtureData fixInContactData;
            AdditionalFixtureData sensorData = (AdditionalFixtureData)sensorFix.UserData;
            if (sensorData.isTouching == true)
            {
                foreach (Fixture fixInContact in sensorData.fixInContactList)
                {
                    if (fixInContact.UserData == null)
                        continue;
                   fixInContactData = (AdditionalFixtureData)fixInContact.UserData;
                    if (fixInContactData.physicParent is Player && activated == true)
                        applyAttraction(fixInContactData.physicParent.body);
                    if (fixInContactData.physicParent is RandomGarbage && activated == true)
                        applyAttraction(fixInContactData.physicParent.body);
                }
            }
        }
    }
}
