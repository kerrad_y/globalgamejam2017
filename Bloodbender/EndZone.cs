﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Bloodbender
{
    class EndZone : PhysicObj
    {
        bool mapload;
        Fixture zoneFixture;
        public EndZone(Vector2 position, float width, float height) : base(position)
        {
            mapload = false;
            zoneFixture = createRectangleFixture(width, height, Vector2.Zero, 0, new AdditionalFixtureData(this, HitboxType.ATTACK));
            zoneFixture.IsSensor = true;
            addFixtureToCheckedCollision(zoneFixture);
            setBodyType(BodyType.Static);
        }

        public override bool Update(float elapsed)
        {
            checkSensorInteractions();
            return base.Update(elapsed);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        private void checkSensorInteractions()
        {
            AdditionalFixtureData fixInContactData;
            AdditionalFixtureData sensorData = (AdditionalFixtureData)zoneFixture.UserData;
            if (sensorData.isTouching == true) {
            
                foreach (Fixture fixInContact in sensorData.fixInContactList)
                {
                    if (fixInContact.UserData == null)
                        continue;
                    fixInContactData = (AdditionalFixtureData)fixInContact.UserData;
                    if (fixInContactData.physicParent is Player)
                    {
                        if (!mapload)
                        {
                            mapload = true;
                            Bloodbender.ptr.map.loadMap();
                            break;
                        }
                    }
                }
            }
        }
    }
}
