﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Collision;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common.TextureTools;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.ConvexHull;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Bloodbender
{
    public class Player : PhysicObj
    {
        List<Fixture> playerBoundsFix;
        List<Fixture> playerHitSensorFix;
        float attackSensorAngle;
        bool dead;

        public Player(Vector2 position) : base(position)
        {
            Bloodbender.ptr.camera.TrackingBody = body;

            playerBoundsFix = new List<Fixture>();

            dead = false;
            velocity = 200;
            attackSensorAngle = 0f;

            //playerBoundsFix = createRectangleFixture(32.0f, 32.0f, Vector2.Zero, 1, new AdditionalFixtureData(this, HitboxType.BOUND));
            Vertices mapVertices = new Vertices();

            mapVertices.Add(new Vector2(0 - 32, 16 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(16 - 32, 32 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(48 - 32, 32 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(64 - 32, 16 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(50 - 32, 8 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(32 - 32, 16 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(16 - 32, 8 - 16) * Bloodbender.pixelToMeter);
            mapVertices.Add(new Vector2(10 - 32, 8 - 16) * Bloodbender.pixelToMeter);

            List<Vertices> listVertice = Triangulate.ConvexPartition(mapVertices, TriangulationAlgorithm.Bayazit);

            foreach (Vertices vertice in listVertice) {
                PolygonShape shape = new PolygonShape(vertice, 1);
                Fixture fix = body.CreateFixture(shape);
                fix.UserData = new AdditionalFixtureData(this, HitboxType.BOUND);
                playerBoundsFix.Add(fix);
                addFixtureToCheckedCollision(fix);
            }
            //playerBoundsFix = body.CreateFixture(shape);

            //plau

            //playerHitSensorFix
            
            //shapeFix.UserData = new AdditionalFixtureData(this, HitboxType.BOUND);


            body.FixedRotation = false;
            body.Restitution = 0.1f;
            body.Friction = 4;

            //add method to be called on collision, different denpending of fixture
           
        }

        public override bool Update(float elapsed)
        {
            if (dead == true)
                return false;
            float pixelToMeter = Bloodbender.pixelToMeter;
            int nbrArrowPressed = 0;
            /*
            if (Keyboard.GetState().IsKeyDown(Keys.Z)
                || Keyboard.GetState().IsKeyDown(Keys.S)
                || Keyboard.GetState().IsKeyDown(Keys.Q)
                || Keyboard.GetState().IsKeyDown(Keys.D))
                body.LinearVelocity = new Vector2(0, 0);

            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                if (!Keyboard.GetState().IsKeyDown(Keys.S))
                {
                    nbrArrowPressed += 1;                    
                    body.LinearVelocity += new Vector2(0, -velocity * pixelToMeter);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                nbrArrowPressed += 1;
                body.LinearVelocity += new Vector2(0, velocity * pixelToMeter);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                if (!Keyboard.GetState().IsKeyDown(Keys.Q))
                {
                    nbrArrowPressed += 1;
                    body.LinearVelocity += new Vector2(velocity * pixelToMeter, 0);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                nbrArrowPressed += 1;
                body.LinearVelocity += new Vector2(-velocity * pixelToMeter, 0);
            }
            if (nbrArrowPressed >= 2)
                body.LinearVelocity /= 2;
                */

            //checkSensorInteractions();

            return base.Update(elapsed);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        private void checkSensorInteractions()
        {
            /*
            if (!Keyboard.GetState().IsKeyDown(Keys.Space))
                return;
            AdditionalFixtureData fixInContactData;
            AdditionalFixtureData sensorData = (AdditionalFixtureData)playerHitSensorFix.UserData;
            if (sensorData.isTouching == true)
            {
                foreach (Fixture fixInContact in sensorData.fixInContactList)
                {
                    if (fixInContact.UserData == null)
                        continue;
                    fixInContactData = (AdditionalFixtureData)fixInContact.UserData;
                }
            }*/
        }

        public void death()
        {

            SoundEffectInstance cryInstance = Bloodbender.ptr.cryCanEffect.CreateInstance();
            SoundEffectInstance expInstance = Bloodbender.ptr.explosionCanEffect.CreateInstance();
            cryInstance.Volume = 0.3f;
            expInstance.Volume = 0.3f;
            cryInstance.Play();
            expInstance.Play();

            foreach (Fixture fix in playerBoundsFix)
            {
                fix.Dispose();
            }
            body.Dispose();
            if (dead == false)
            {
                Random r = new Random();
                for (int i = 0; i < 50; i++)
                {
                    RandomGarbage garbage = new RandomGarbage(position);
                    garbage.body.LinearVelocity = new Vector2(r.Next(-30, 30), r.Next(-30, 30));
                    garbage.body.Friction = 5;
                    garbage.body.Restitution = 0.3f;
                    garbage.body.AngularDamping = 0.3f;
                }
            }
            dead = true;
        }
    }
}
