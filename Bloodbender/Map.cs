﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TiledSharp;

namespace Bloodbender
{
    public class Map
    {

        private List<String> maps;
        private int nextMap;
        private Texture2D carre;
        private Texture2D carre2;

        private Vector2 spawnPoint;

        public Map(Texture2D carre, Texture2D carre2)
        {
            maps = new List<String>();
            maps.Add("./../../../../Content/someMap-1.tmx");
            maps.Add("./../../../../Content/someMap.tmx");
            maps.Add("./../../../../Content/someMap2.tmx");
            maps.Add("./../../../../Content/someMap3.tmx");
            maps.Add("./../../../../Content/someMap5.tmx");
            maps.Add("./../../../../Content/someMap4.tmx");
            
            //toremove
            this.carre = carre;
            this.carre2 = carre2; 
            spawnPoint = new Vector2(0, 0);
            nextMap = 0;
        }

        public void loadMap(bool reload = false)
        {
            if (reload == true)
                nextMap -= 1;
            if (nextMap >= maps.Count())
                nextMap = 0;
            else
            {
                SoundEffectInstance soundInstance = Bloodbender.ptr.openCanEffect.CreateInstance();
                soundInstance.Volume = 0.3f;
                soundInstance.Play();
            }
            Bloodbender.ptr.listGraphicObj.Clear();
            Bloodbender.ptr.world.Clear();
            this.startLoad(maps[nextMap]);
            nextMap += 1;
        }

        public void startLoad(String path)
        {
            TmxMap map = new TmxMap(path);
            this.loadTextures(map);
            this.loadPhysics(map);
            
        }

        public void loadTextures(TmxMap map)
        {
            TmxList<TmxTileset> tileSets = map.Tilesets;
            TmxList<TmxLayer> layers = map.Layers;
            float profondeur = 0.0000000000015f;
            foreach (TmxLayer layer in layers)
            {
                Collection<TmxLayerTile> tileLayers = layer.Tiles;
                foreach (TmxLayerTile tileLayer in tileLayers)
                {
                    int i = 0;
                    TmxTileset tileSource = null;

                    while (i < tileSets.Count && tileSource == null)
                    {
                        if (tileLayer.Gid >= tileSets[i].FirstGid)
                        {
                            if (i + 1 >= tileSets.Count)
                                tileSource = tileSets[i];
                            else if (i + 1 < tileSets.Count && tileLayer.Gid < tileSets[i + 1].FirstGid)
                                tileSource = tileSets[i];
                        }
                        i++;
                    }
                    if (tileSource != null) {
                        int pos = (tileLayer.Gid - tileSource.FirstGid + 1);

                        GraphicObj elem = new GraphicObj();
                        elem.position = new Vector2(tileLayer.X * map.TileWidth, tileLayer.Y * map.TileHeight);

                        Animation anim = new Animation(Bloodbender.ptr.Content.Load<Texture2D>(Path.GetFileNameWithoutExtension(tileSource.Image.Source)), map.TileWidth, map.TileHeight, (int)(pos / tileSource.Columns), (int)(pos % tileSource.Columns) - 1);
                        anim.forceDepth(profondeur);//bidoulliage
                        elem.addAnimation(anim);

                        Bloodbender.ptr.listGraphicObj.Add(elem);
                    }
                }
                profondeur += 0.0000000000001f;
            }
        }

        public void loadPhysics(TmxMap map)
        {
            TmxList<TmxObjectGroup> objects = map.ObjectGroups;
            TmxObjectGroup spawn = objects["spawn"];
            this.loadSpawn(spawn);
            TmxObjectGroup end = objects["end"];
            this.loadEnd(end);
            TmxObjectGroup deathwalls = objects["deathwalls"];
            this.loadDeathWalls(deathwalls);
            TmxObjectGroup platforms = objects["platforms"];
            this.loadPlatforms(platforms);
            TmxObjectGroup magnets = objects["magnets"];
            this.loadMagnets(magnets);
        }

        public void loadSpawn(TmxObjectGroup spawnG)
        {
            if (spawnG.Objects.Count > 0) {
                TmxObject spawn = spawnG.Objects[0];
                this.spawnPoint.X = (float)spawn.X;
                this.spawnPoint.Y = (float)spawn.Y;
                Player player = new Player(new Vector2((float)spawn.X, (float)spawn.Y));
                Texture2D can = Bloodbender.ptr.Content.Load<Texture2D>("can");
                player.addAnimation(new Animation(can));
                Bloodbender.ptr.listGraphicObj.Add(player);
                
            }
        }

        public void loadEnd(TmxObjectGroup endG)
        {
            if (endG.Objects.Count > 0)
            {
                TmxObject spawn = endG.Objects[0];
                EndZone zone = new EndZone(new Vector2((float)(spawn.X + (spawn.Width / 2)), (float)(spawn.Y + (spawn.Height / 2))), (float)(spawn.Width), (float)(spawn.Height));
                Bloodbender.ptr.listGraphicObj.Add(zone);
            }
        }

        public void loadDeathWalls(TmxObjectGroup deathwallsG)
        {
            TmxObject deathwall = deathwallsG.Objects[0];
            Collection<TmxObjectPoint> points = deathwall.Points;
            Vector2 two = Vector2.Zero;
            Vector2 one = Vector2.Zero;

            MapBound mapBounds = new MapBound();
            int i = 0;
            foreach (TmxObjectPoint point in points) {
                if (i < points.Count - 1)
                    mapBounds.addVertex(new Vector2((float)(point.X + deathwall.X), (float)(point.Y + deathwall.Y)));
                i++;
            }
            mapBounds.finiliezMap();
            Bloodbender.ptr.listGraphicObj.Add(mapBounds);
        }

        public void loadPlatforms(TmxObjectGroup platformsG)
        {
            TmxList<TmxObject> platforms = platformsG.Objects;

            foreach (TmxObject platform in platforms)
            {
                PhysicObj phy = new PhysicObj(new Vector2((float)(platform.X + (platform.Width / 2)), (float)(platform.Y + (platform.Height / 2))));
                phy.setBodyType(FarseerPhysics.Dynamics.BodyType.Static);
                phy.body.Friction = 4;
                phy.createRectangleFixture((float)(platform.Width), (float)(platform.Height), Vector2.Zero, 1);
            }

        }

        public void loadMagnets(TmxObjectGroup magnetsG)
        {
            TmxList<TmxObject> magnets = magnetsG.Objects;

            foreach (TmxObject magnet in magnets)
            {
                Magnet phyMag = new Magnet(new Vector2((float)(magnet.X + (magnet.Width / 2)), (float)(magnet.Y + (magnet.Height / 2))), (float)(magnet.Width / 2) * Bloodbender.pixelToMeter, float.Parse(magnet.Properties["force"]));

                phyMag.range.scale = new Vector2(
                                                    (float)(magnet.Width / phyMag.range.getAnimation(phyMag.range.getCurrentAnimation()).frameWidth),
                                                    (float)(magnet.Height / phyMag.range.getAnimation(phyMag.range.getCurrentAnimation()).frameHeight)

                                                    );

                phyMag.wave.scale = new Vector2(
                                                    (float)(magnet.Width / phyMag.wave.getAnimation(phyMag.wave.getCurrentAnimation()).frameWidth),
                                                    (float)(magnet.Height / phyMag.wave.getAnimation(phyMag.wave.getCurrentAnimation()).frameHeight)

                                                    );

                phyMag.addAnimation(new Animation(carre2));
                Bloodbender.ptr.listGraphicObj.Add(phyMag);
            }
        }
    }
}
