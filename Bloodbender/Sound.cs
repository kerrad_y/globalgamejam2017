﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bloodbender
{
    public static class Sound
    {

        public static void playRandomSound()
        {

            int soundItr = Bloodbender.ptr.random.Next(0, Bloodbender.ptr.canHitEffects.Count - 1);

            SoundEffectInstance soundInstance = Bloodbender.ptr.canHitEffects[soundItr].CreateInstance();

            soundInstance.Volume = (float)0.1;
            soundInstance.Play();
        }

    }
}
