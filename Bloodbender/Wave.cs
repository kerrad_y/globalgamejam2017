﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bloodbender
{
    public class Wave : GraphicObj
    {
        public Wave(GraphicObj.OffSet offSet) : base(offSet)
        {
            Bloodbender.ptr.listGraphicObj.Add(this);

            Texture2D wavemin = Bloodbender.ptr.Content.Load<Texture2D>("ondeMinus");
            Texture2D waveplus = Bloodbender.ptr.Content.Load<Texture2D>("ondePlus");

            Animation animwavemin = new Animation(wavemin, 5, 0.05f);
            Animation animwaveplu = new Animation(waveplus, 5, 0.05f);

            addAnimation(animwaveplu);
            addAnimation(animwavemin);

            runAnimation(0);
        }
        public override bool Update(float elapsed)
        {
            rotation += 0.01f;

            return base.Update(elapsed);
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

    }
}
