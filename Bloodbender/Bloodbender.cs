﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using FarseerPhysics.DebugView;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using System;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Bloodbender
{
    public class Bloodbender : Game
    {
        public static Bloodbender ptr { get; set; }

        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        //public ResolutionIndependentRenderer resolutionIndependence;
        public Camera camera;

        public World world;
        public const float meterToPixel = 32;
        public const float pixelToMeter = 1 / meterToPixel;
        public DebugViewXNA debugView;


        public InputHelper inputHelper;

        public List<GraphicObj> listGraphicObj;

        public float elapsed = 0.0f;

        public Texture2D bouleRouge;

        public Map map;

        public List<SoundEffect> canHitEffects;
        public SoundEffect openCanEffect;
        public SoundEffect explosionCanEffect;
        public SoundEffect cryCanEffect;
        public SoundEffect attractCanEffect;
        public SoundEffect repulseCanEffect;

        public Random random;

        public Bloodbender()
        {
            ptr = this;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

          
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            //graphics.IsFullScreen = true;
   

            //resolutionIndependence = new ResolutionIndependentRenderer(this);            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            world = new World(new Vector2(0, 4));

            ConvertUnits.SetDisplayUnitToSimUnitRatio(meterToPixel);

            debugView = new DebugViewXNA(world);
            debugView.RemoveFlags(DebugViewFlags.Shape);
            debugView.RemoveFlags(DebugViewFlags.Joint);
            debugView.DefaultShapeColor = Color.White;
            debugView.SleepingShapeColor = Color.LightGray;
            debugView.LoadContent(GraphicsDevice, Content);

            inputHelper = new InputHelper();
            inputHelper.ShowCursor = true;

            listGraphicObj = new List<GraphicObj>();

            //InitializeResolutionIndependence(graphics.GraphicsDevice.Viewport.Width, graphics.GraphicsDevice.Viewport.Height);

            camera = new Camera(GraphicsDevice);
            camera.Zoom = 0.55f;

            spriteBatch = new SpriteBatch(GraphicsDevice);

            canHitEffects = new List<SoundEffect>();

            random = new Random();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {

            // Create a new SpriteBatch, which can be used to draw textures.
            inputHelper.LoadContent();

            Texture2D textureCarre = Content.Load<Texture2D>("carre");
            Texture2D textureCarre2 = Content.Load<Texture2D>("fromMinusToPlus");
            Texture2D textureTotem = Content.Load<Texture2D>("Totem");
            Texture2D bouleRouge = Content.Load<Texture2D>("fromMinusToPlus");
            
            openCanEffect = Content.Load<SoundEffect>("openCan");
            explosionCanEffect = Content.Load<SoundEffect>("explosionCan");
            cryCanEffect = Content.Load<SoundEffect>("cryCan");

            attractCanEffect = Content.Load<SoundEffect>("attractCan");
            repulseCanEffect = Content.Load<SoundEffect>("repulseCan");

            canHitEffects.Add(Content.Load<SoundEffect>("hitCan"));
            canHitEffects.Add(Content.Load<SoundEffect>("hitCan2"));
            canHitEffects.Add(Content.Load<SoundEffect>("hitCan3"));
            canHitEffects.Add(Content.Load<SoundEffect>("hitCan4"));
            canHitEffects.Add(Content.Load<SoundEffect>("hitCan5"));

            map = new Map(textureCarre, textureCarre2);
            map.loadMap();

            //Song song = Content.Load<Song>("FUELTANK");
            //MediaPlayer.Volume = (float)0.3;
            //MediaPlayer.Play(song);
            /*
            PhysicObj pobj = new PhysicObj(BodyFactory.CreateRectangle(world, 32 * pixelToMeter, 32 * pixelToMeter, 1), // meterTopixel a la place de 32?
                new Vector2(200, 200));
            pobj.addAnimation(new Animation(textureCarre2));
            pobj.isRotationFixed(true);
            */
            //Magnet magnet = new Magnet(new Vector2(300, 300), 500 * pixelToMeter, 200);
            //Magnet magnet2 = new Magnet(new Vector2(1000, 300), 300 * pixelToMeter, 100);
            //EndZone zone = new EndZone(new Vector2(400, 300), 100, 100);

            /*
            //for (int i = 0; i < 20; i++)
            //{
                RandomGarbage gar = new RandomGarbage();
                listGraphicObj.Add(gar);
                //Console.WriteLine("test" + i);
            //}*/

            //listGraphicObj.Add(magnet);
            //listGraphicObj.Add(magnet2);

        }
        /*
        private void InitializeResolutionIndependence(int realScreenWidth, int realScreenHeight)
        {
            resolutionIndependence.VirtualWidth = 1280;
            resolutionIndependence.VirtualHeight = 720;
            resolutionIndependence.ScreenWidth = realScreenWidth;
            resolutionIndependence.ScreenHeight = realScreenHeight;
            resolutionIndependence.Initialize();
        }
        */

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            inputHelper.Update(elapsed);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (inputHelper.IsNewKeyPress(Keys.R))
                map.loadMap(true);

            world.Step(1f / 30f);

            List<int> indexToDelete = new List<int>();
            for (int i = 0; i < listGraphicObj.Count; ++i)
            {
                if (listGraphicObj[i].Update(elapsed) == false)
                    indexToDelete.Add(i);
            }

            for (int i = 0; i < indexToDelete.Count; i++)
            {
                listGraphicObj.RemoveAt(indexToDelete[i]);
            }

            camera.Update(elapsed);


            if (inputHelper.IsNewKeyPress(Keys.F1))
                debugView.EnableOrDisableFlag(DebugViewFlags.Shape);
            if (inputHelper.IsNewKeyPress(Keys.F2))
            {
                debugView.EnableOrDisableFlag(DebugViewFlags.DebugPanel);
                debugView.EnableOrDisableFlag(DebugViewFlags.PerformanceGraph);
            }
            if (inputHelper.IsNewKeyPress(Keys.F3))
                debugView.EnableOrDisableFlag(DebugViewFlags.Joint);
            if (inputHelper.IsNewKeyPress(Keys.F4))
            {
                debugView.EnableOrDisableFlag(DebugViewFlags.ContactPoints);
                debugView.EnableOrDisableFlag(DebugViewFlags.ContactNormals);
            }
            if (inputHelper.IsNewKeyPress(Keys.F5))
                debugView.EnableOrDisableFlag(DebugViewFlags.PolygonPoints);
            if (inputHelper.IsNewKeyPress(Keys.F6))
                debugView.EnableOrDisableFlag(DebugViewFlags.Controllers);
            if (inputHelper.IsNewKeyPress(Keys.F7))
                debugView.EnableOrDisableFlag(DebugViewFlags.CenterOfMass);
            if (inputHelper.IsNewKeyPress(Keys.F8))
                debugView.EnableOrDisableFlag(DebugViewFlags.AABB);

            if (inputHelper.IsNewKeyPress(Keys.F12))
                graphics.ToggleFullScreen();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //resolutionIndependence.BeginDraw();
            graphics.GraphicsDevice.Clear(Color.Gray);

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.View);

                        
            for (int i = 0; i < listGraphicObj.Count; ++i)
                listGraphicObj[i].Draw(spriteBatch);

            spriteBatch.End();
   

            debugView.RenderDebugData(ref camera.SimProjection, ref camera.SimView);

            inputHelper.Draw();


            base.Draw(gameTime);
        }

        
    }
}
